﻿using AutoMapper;
using Dominio;

namespace WebAPI.DTOs
{
    public class ApplicationProfile : Profile
    {
        //Se van a mapear las propiedades 
        public ApplicationProfile()
        {
            CreateMap<ExchangeRate, ExchangeRateDTO>().ReverseMap();
        }
    }
}
