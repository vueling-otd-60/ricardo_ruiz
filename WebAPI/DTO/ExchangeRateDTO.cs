﻿namespace WebAPI.DTOs
{
    public class ExchangeRateDTO
    {
        public string Amount { get; set; }
        public string Currency { get; set; }
    }
}
