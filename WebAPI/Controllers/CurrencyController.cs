﻿using Aplicacion.ManejadorError;
using Aplicacion.Services.Contratos;
using AutoMapper;
using Dominio;
using Microsoft.AspNetCore.Mvc;
using Persistencia.Contratos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using WebAPI.DTOs;

namespace WebAPI.Controllers
{
    //Como ya sabemos los controles : Fueron creados simplemente para recibir request de Clientes, esa es su principal función.
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        //private readonly IDatosPrueba _datosPrueba;
        private readonly IDataPrueba _datosPrueba;
        private readonly IMapper _mapper;
        private readonly ILoggerManager _logger;

        public CurrencyController(IDataPrueba datosPrueba, IMapper mapper, ILoggerManager logger) //Creamos la inyección de dependencia.
        {
            _datosPrueba = datosPrueba;
            _mapper = mapper;
            _logger = logger;
        }

        //Método 1: Listado de todas las transacciones.

        // GET: api/Currency/GetAll
        [HttpGet("[action]")]
        public IEnumerable<ExchangeRate> GetAll()
        {
            return _datosPrueba.ListarMonedas();
        }

        //Método 2: Listado de obtener todos los rates.

        // GET: api/Currency/GetRates
        [HttpGet("[action]")]
        public IEnumerable<ExchangeRateDTO> GetRates()
        {
            try
            {
                //Mapeamos las propiedades para que devuelve solo los rates: Amount y Currency.
                return _mapper.Map<IEnumerable<ExchangeRateDTO>>(_datosPrueba.ListarMonedas());
            }
            catch (Exception)
            {
                //En caso de error me devuelve el código que me devuelve el error.
                _logger.LogInfo("Fallo en la respuesta");
                throw new ManejadorExcepcion(HttpStatusCode.BadRequest, new { mensaje = "Error en el modelo." });
            }
        }

        //POST: api/Currency/SearchSku
        [HttpPost("[action]")]
        public IEnumerable<ExchangeRate> SearchSku([FromBody] ExchangeRate model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!ModelState.IsValid)
                    {
                        _logger.LogInfo("Error en el modelo.");
                        throw new ManejadorExcepcion(HttpStatusCode.BadRequest, new { mensaje = "Error en el modelo." });
                    }
                }
                //Extraemos los SKU que le pase el cliente 
                var listado = _datosPrueba.ListarMonedas().Where(x => x.Sku == model.Sku && x.Currency == "EUR").ToList();

                var result = from item in listado
                             group item by item.Sku into g
                             select new ExchangeRate()
                             {
                                 Sku = g.Key,
                                 Amount = g.Sum(x => x.Amount)
                             };

                //Mapeamos las propiedades para que devuelve solo los rates: Amount y Currency.
                return _mapper.Map<IEnumerable<ExchangeRate>>(result);
            }
            catch (Exception)
            {
                //En caso de error me devuelve el código y el mensaje de error.
                _logger.LogInfo("No se encontro la moneda");
                throw new ManejadorExcepcion(HttpStatusCode.NotFound, new { mensaje = "No se encontro la moneda" });

            }
        }
    }
}
