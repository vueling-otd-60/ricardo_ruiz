﻿namespace Dominio
{
    public class ExchangeRate
    {
        public string Sku { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
