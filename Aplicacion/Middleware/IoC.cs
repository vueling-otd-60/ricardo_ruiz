﻿using Aplicacion.ManejadorError;
using Aplicacion.Services.Contratos;
using Microsoft.Extensions.DependencyInjection;
using Persistencia;
using Persistencia.Contratos;

namespace Aplicacion.Middleware
{
    public static class IoC
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            //Registramos todos nuestros servicios. Para un mayor control en el Startup y mayor limpieza
            //Para luego hacer la inyección de dependencia en los controles correspondientes.
            services.AddSingleton<IDataPrueba, DataPrueba>();

            services.AddSingleton<ILoggerManager, LoggerManager>();

            services.AddControllers();

            return services;
        }
    }
}
