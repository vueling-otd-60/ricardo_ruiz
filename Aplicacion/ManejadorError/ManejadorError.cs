﻿using System;
using System.Net;

namespace Aplicacion.ManejadorError
{
    //Creamos nuestro manejador de errores
    public class ManejadorExcepcion : Exception
    {
        public HttpStatusCode Codigo { get; }
        public object Errores { get; }
        public ManejadorExcepcion(HttpStatusCode codigo, object errores = null)
        {
            Codigo = codigo;
            Errores = errores;
        }
    }
}