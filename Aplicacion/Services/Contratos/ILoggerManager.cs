﻿namespace Aplicacion.Services.Contratos
{
    public interface ILoggerManager
    {
        //Interfaz para crear nuestros metodos para mostrar los diferentes logs
        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
    }
}
