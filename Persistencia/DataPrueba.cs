﻿using Dominio;
using Newtonsoft.Json;
using Persistencia.Contratos;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Persistencia
{
    public class DataPrueba : IDataPrueba
    {
        public IEnumerable<ExchangeRate> ListarMonedas()
        {
            var resultMoneda = new List<ExchangeRate>();

            using var wc = new WebClient();
            //Consultamos el Endpoint del Webservice
            string DATA = wc.DownloadString($"http://quiet-stone-2094.herokuapp.com/transactions.json");

            //Creamos un json para practicar la Peristencia 
            string readJson = File.ReadAllText("wwwroot/json/jmoneda.json");
            if (string.IsNullOrEmpty(readJson))
            {
                //Creamos la peristencia en caso de que falle el webservice
                File.WriteAllText(@"wwwroot/json/jmoneda.json", DATA);
                List<ExchangeRate> resultado = JsonConvert.DeserializeObject<List<ExchangeRate>>(DATA);
                return resultado;
            }
            else
            {
                //Si json no esta vacío se trabaja con esos datos. Ambiente ficticio. En produccion se consulta directamente a la base de datos
                List<ExchangeRate> resultado = JsonConvert.DeserializeObject<List<ExchangeRate>>(readJson);
                return resultado;
            }

        }
    }
}

