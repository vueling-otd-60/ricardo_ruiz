﻿using Dominio;
using System.Collections.Generic;

namespace Persistencia.Contratos
{
    public interface IDataPrueba
    {
        public IEnumerable<ExchangeRate> ListarMonedas();
    }
}
